
var history = function(){
  return React.createClass({
  render:function(){
    //debugger;
      var orderRows = this.props.orders.map(function  (order) {
        //debugger;
        return(
          React.DOM.tr(null,  
            React.DOM.td(null,moment(+order.createdDate).toLocaleString()),
            React.DOM.td(null,order.product.price),
            React.DOM.td(null,order.result)
            )
          );
      })
      return(
        React.DOM.div({className: "small-12 columns"},
          React.DOM.table(null,
            React.DOM.thead(null,
              React.DOM.tr(null,
                React.DOM.th({width:200},"date"),
                React.DOM.th({width:100},"price"),
                React.DOM.th({width:1000},"result"))),
            React.DOM.tbody(null,
              React.DOM.tr(null,
                orderRows ))
            )
          )
        );
    }
  });
}
