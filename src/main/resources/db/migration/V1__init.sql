do $$
begin

 create table order_result (order_id int8 not null, result int4);
 create table sorting_order (id int8 not null, created_date timestamp, user_id int8, primary key (id));
 create table sorting_user (id int8 not null, enable boolean, password varchar(255) not null, username varchar(255) not null, primary key (id));
 create table sorting_user_orders (sorting_user_id int8 not null, orders_id int8 not null);
 create table sorting_user_roles (sorting_user_id int8 not null, roles_id int8 not null);
 create table tbl_products (id int8 not null, products varchar(255) not null);
 create table user_role (id int8 not null, role varchar(255), username varchar(255), primary key (id));
 alter table sorting_user add constraint UK_aiqyehs85a69fi2wu2wqw9y2u  unique (username);
 alter table sorting_user_orders add constraint UK_ghirmra0v8weuj6jpspredatm  unique (orders_id);
 alter table sorting_user_roles add constraint UK_jolkamrcwa0bv7gaxot8kkjp  unique (roles_id);
 alter table order_result add constraint FK_drrot6n6xvbyg9k6mf1brsnip foreign key (order_id) references sorting_order;
 alter table sorting_order add constraint FK_6n4vk7wt7j3b60ixx586fg6se foreign key (user_id) references sorting_user;
 alter table sorting_user_orders add constraint FK_ghirmra0v8weuj6jpspredatm foreign key (orders_id) references sorting_order;
 alter table sorting_user_orders add constraint FK_lyrl7vtbhvt8unw51u4c5iyel foreign key (sorting_user_id) references sorting_user;
 alter table sorting_user_roles add constraint FK_jolkamrcwa0bv7gaxot8kkjp foreign key (roles_id) references user_role;
 alter table sorting_user_roles add constraint FK_c2b0rbxs3b7vofxe4pew7cb52 foreign key (sorting_user_id) references sorting_user;
 alter table tbl_products add constraint FK_juyo6a0da1dwoukekd812ftee foreign key (id) references sorting_order;
 create sequence hibernate_sequence;
end 
  $$;
do $$
declare
 user_id BIGINT := nextval ('hibernate_sequence');
 role_id BIGINT:= nextval ('hibernate_sequence');
  begin 
insert into sorting_user (enable, password, username, id) values (true, '$2a$04$TEGv5QwnYBkiILgbYKcAqeETWUUUS7b.powKYYnj8Tok0n1z15UNC','admin', user_id);
insert into user_role (role, username, id) values ('ROLE_ADMIN', 'admin', role_id);
insert into sorting_user_roles (sorting_user_id, roles_id) values (user_id, role_id);
end
$$;