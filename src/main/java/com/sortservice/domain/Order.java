package com.sortservice.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
* Created by nick on 12.08.14.
*/
@Entity(name = "sorting_order")
public class Order extends AbstractPersistable<Long> {
    public Order(List<Product> products,  Date createdDate, List<Integer> result) {
        this.products = products;
        this.createdDate = createdDate;
        this.result = result;
    }

    public Order() {
    }

    public Collection<Product> getProducts() {
        return products;
    }

    public void setProducts(Collection<Product> products) {
        this.products = products;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public List<Integer> getResult() {
        return result;
    }

    public void setResult(List<Integer> result) {
        this.result = result;
    }

    @JsonFormat(shape = JsonFormat.Shape.OBJECT)
    public enum Product{
        BUBBLESORT("bubble sort",new BigDecimal(100), "Bubble sort, sometimes referred to as sinking sort, is a simple sorting algorithm that works by repeatedly stepping through the list to be sorted, comparing each pair of adjacent items and swapping them if they are in the wrong order. The pass through the list is repeated until no swaps are needed, which indicates that the list is sorted. The algorithm gets its name from the way smaller elements \"bubble\" to the top of the list. Because it only uses comparisons to operate on elements, it is a comparison sort. Although the algorithm is simple, most of the other sorting algorithms are more efficient for large lists.Bubble sort has worst-case and average complexity both О(n2), where n is the number of items being sorted."),
        MERGESORT("merge sort",new BigDecimal(200), "In computer science, merge sort (also commonly spelled mergesort) is an O(n log n) comparison-based sorting algorithm. Most implementations produce a stable sort, which means that the implementation preserves the input order of equal elements in the sorted output. Mergesort is a divide and conquer algorithm that was invented by John von Neumann in 1945.[1] A detailed description and analysis of bottom-up mergesort appeared in a report by Goldstine and Neumann as early as 1948.[2]"),
        LSDSORT("lsd sort",new BigDecimal(1000), "In computer science, radix sort is a non-comparative integer sorting algorithm that sorts data with integer keys by grouping keys by the individual digits which share the same significant position and value. A positional notation is required, but because integers can represent strings of characters (e.g., names or dates) and specially formatted floating point numbers, radix sort is not limited to integers. Radix sort dates back as far as 1887 to the work of Herman Hollerith on tabulating machines.The topic of the efficiency of radix sort compared to other sorting algorithms is somewhat tricky and subject to quite a lot of misunderstandings. Whether radix sort is equally efficient, less efficient or more efficient than the best comparison-based algorithms depends on the details of the assumptions made. Radix sort efficiency is O(d·n) for n keys which have d or fewer digits. Sometimes d is presented as a constant, which would make radix sort better (for sufficiently large n) than the best comparison-based sorting algorithms, which are all O(n·log(n)) number of comparisons needed. However, in general d cannot be considered a constant. In particular, under the common (but sometimes implicit) assumption that all keys are distinct, then d must be at least of the order of log(n), which gives at best (with densely packed keys) a time complexity O(n·log(n)). That would seem to make radix sort at most equally efficient as the best comparison-based sorts (and worse if keys are much longer than log(n)).");

        private final BigDecimal price;
        private final String desc;
        private final String name;

        public String getName() {
            return name;
        }
        public BigDecimal getPrice() {
            return price;
        }

        public String getDesc() {
            return desc;
        }

        Product(String name,BigDecimal price, String desc) {
            this.price = price;
            this.desc = desc;
            this.name = name;
        }
    }

    @ElementCollection(targetClass = Product.class)
    @JoinTable(name = "tblProducts", joinColumns = @JoinColumn(name = "id"))
    @Column(name = "products", nullable = false)
    @Enumerated(EnumType.STRING)
    private Collection<Product> products;

//    @CreatedDate
//    private LocalDateTime createdDate;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDate;

    @ElementCollection
    private List<Integer> result;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @ManyToOne
    @JsonIgnore
    private User user;
}
