package com.sortservice.domain;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by nick on 12.08.14.
 */
public interface UserRepository extends CrudRepository<User,Long> {
    User findByUsername(String username);

}
