package com.sortservice.domain;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;

/**
* Created by nick on 12.08.14.
*/
public interface OrderRepository extends CrudRepository<Order,Long> {
    @Query("select o from sorting_order o where o.user.id = ?1")
    Iterable<Order> findByUserId(Long id);
    @Query("select o from sorting_order o where o.createdDate between :start and :end")
    Iterable<Order> findBetweenDates(@Param("start")Date start ,@Param("end") Date end);
}