package com.sortservice.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by nick on 12.08.14.
 */
@Entity(name = "sorting_user")
@Table(uniqueConstraints = @UniqueConstraint(columnNames = "username"))
public class User extends AbstractPersistable<Long> {

    public User() {
    }

    @Override
    protected void setId(Long id) {
        super.setId(id);
    }

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private Boolean enable;

    @OneToMany(cascade = {CascadeType.PERSIST})
    private List<Order> orders = new ArrayList<>();

    @OneToMany(cascade = {CascadeType.PERSIST},fetch = FetchType.EAGER)
    private List<UserRole> roles;

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public List<Order> getOrders() {

        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User(String username, String password, Boolean enable) {
        this.username = username;
        this.password = password;
        this.enable = enable;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<UserRole> getRoles() {
        return roles;
    }

    public void setRoles(List<UserRole> roles) {
        this.roles = roles;
    }
}
