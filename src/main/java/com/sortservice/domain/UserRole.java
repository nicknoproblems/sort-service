package com.sortservice.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.Entity;

/**
 * Created by nick on 12.08.14.
 */
@Entity
public class UserRole extends AbstractPersistable<Long> {
    public UserRole(String role, String username) {
        this.role = role;
        this.username = username;
    }

    public UserRole() {
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    private String role = "ROLE_USER";

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
