package com.sortservice.config;

import com.sortservice.domain.User;
import com.sortservice.domain.UserRepository;
import com.sortservice.domain.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nick on 12.08.14.
 */
@Configuration
@EnableWebSecurity
public class Security extends WebSecurityConfigurerAdapter {
    @Autowired
    private UserRepository repository;
    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UserRepository userRepository;


    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(new AuthenticationProvider() {
                    @Override
                    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
                        User user = repository.findByUsername(authentication.getName());
                        String pass = (String) authentication.getCredentials();
                        if (null == user || null == pass)
                            throw new UsernameNotFoundException("for user" + authentication.getName());
                        if (encoder.matches(pass, user.getPassword())) {
                            List<UserRole> authorities = user.getRoles();
                            return new UsernamePasswordAuthenticationToken(authentication.getPrincipal(), authentication.getCredentials(),
                                    authorities.stream().map((it) -> new SimpleGrantedAuthority(it.getRole())).collect(Collectors.toList()));
                        } else throw new BadCredentialsException("for user " + authentication.getName());
                    }

                    @Override
                    public boolean supports(Class<?> authentication) {
                        if (authentication.equals(UsernamePasswordAuthenticationToken.class)) return true;
                        return false;
                    }
                });
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin()
                .usernameParameter("login")
                .passwordParameter("pass")
                .loginPage("/login")
                .loginProcessingUrl("/login")
                .defaultSuccessUrl("/index")
                .failureUrl("/login")
                .permitAll()
                .successHandler((HttpServletRequest request, HttpServletResponse response,
                                 Authentication authentication) -> {
                    Long userId = userRepository.findByUsername(authentication.getName()).getId();
                    response.addCookie(new Cookie("userId",userId.toString()));
                    Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
                    if (authorities.contains(new SimpleGrantedAuthority("ROLE_USER")))
                        response.sendRedirect("/sort-service/index");
                    else if (authorities.contains(new SimpleGrantedAuthority("ROLE_ADMIN")))
                        response.sendRedirect("/sort-service/admin");

                })
                .and()
                .csrf()
                .disable()
                .anonymous()
                .key("anonymKey")
                .and()
                .authorizeRequests()
                .antMatchers("/register")
                .hasAnyRole("ANONYMOUS")
                .and()
                .authorizeRequests()
                .antMatchers("/", "/api/**")
                .hasAnyRole("USER", "ADMIN")
                .and()
                .httpBasic()
                .and()
                .authorizeRequests()
                .antMatchers("/ui/**")
                .hasAnyRole("USER", "ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/admin/api/**")
                .hasAnyRole("ADMIN")
                .and()
                .authorizeRequests()
                .antMatchers("/anonymous/api/**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/static/**")
                .permitAll()
                .and()
                .logout()
                .invalidateHttpSession(true)
                .logoutUrl("/logout")
                .logoutSuccessUrl("/login")
                .permitAll()
                .and()
                .sessionManagement()
                .maximumSessions(1)
                .and()
                .invalidSessionUrl("/login")
                .sessionFixation()
                .newSession();


    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
