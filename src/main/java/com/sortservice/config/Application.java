package com.sortservice.config;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.boot.autoconfigure.security.SpringBootWebSecurityConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.io.File;

import static org.springframework.core.Ordered.HIGHEST_PRECEDENCE;
/**
 * Created by nick on 12.08.14.
 */
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class, SpringBootWebSecurityConfiguration.class})
@ComponentScan(basePackages = {"com.sortservice.*"})
@EnableJpaRepositories("com.sortservice.domain")
@EnableTransactionManagement
@EnableSpringDataWebSupport
@EntityScan("com.sortservice.domain")
@Order(HIGHEST_PRECEDENCE)
public class Application  {

    @Autowired
    private Environment env;

    public static void main(String[] args) {
        ConfigurableApplicationContext ctx = SpringApplication.run(Application.class, args);
        //Arrays.stream(ctx.getBeanDefinitionNames()).forEach( System.out::println);
    }

    @Bean
    public EmbeddedServletContainerFactory servletContainer() {
        TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
        tomcat.addAdditionalTomcatConnectors(createSslConnector());
        return tomcat;
    }

    private Connector createSslConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        Http11NioProtocol protocol = (Http11NioProtocol) connector.getProtocolHandler();
        File keystore = new File(env.getProperty("keystore.location"));
        File truststore = new File(env.getProperty("keystore.location"));
        connector.setScheme("https");
        connector.setSecure(true);
        connector.setPort(Integer.parseInt(env.getProperty("https.port")));
        protocol.setSSLEnabled(true);
        protocol.setKeystoreFile(keystore.getAbsolutePath());
        protocol.setKeystorePass("quicksort");
        protocol.setTruststoreFile(truststore.getAbsolutePath());
        protocol.setTruststorePass("quicksort");
        return connector;

    }
}
