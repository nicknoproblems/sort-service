package com.sortservice.controllers;

import com.sortservice.domain.Order;
import com.sortservice.domain.OrderRepository;
import com.sortservice.domain.User;
import com.sortservice.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.time.Instant;
import java.util.Date;

/**
 * Created by nick on 13.08.14.
 */
@RestController
public class UserController {
    @Autowired
    private UserRepository repository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private PasswordEncoder encoder;

    @RequestMapping(value = "api/user/{id}/order")
    public Iterable<Order> ordersByUserId(@PathVariable Long id,HttpServletResponse res){
        if(checkUser(id)) {
            User user = repository.findOne(id);
            return orderRepository.findByUserId(user.getId());
        }
        res.setStatus(HttpStatus.NOT_FOUND.value());
        return null;
    }

    private boolean checkUser(Long id) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(null != auth){
            return repository.findByUsername(auth.getName()).getId().equals(id) ? true : false;
        }
        return false;
    }

    @RequestMapping(value = "admin/api/user",method = RequestMethod.GET)
    public Iterable<User> all(){
        return repository.findAll();
    }

    @RequestMapping(value = "anonymous/api/user/exists",method = RequestMethod.GET)
    public boolean getUserNames(@RequestParam("name") String name){
        return repository.findByUsername(name) != null ? true:false;
    }

    @RequestMapping(value = "api/product",method = RequestMethod.GET)
    public Order.Product[] getProduct(){
        return Order.Product.values();
    }

    @RequestMapping(value = "admin/api/order/between",method = RequestMethod.GET)
    public Iterable<Order> ordersByDate(@RequestParam Long start, @RequestParam Long end){
        return orderRepository.findBetweenDates(new Date(start),new Date(end));
    }

    @RequestMapping(value = "admin/api/change/password",method = RequestMethod.GET)
    public void chanchePass(@RequestParam String pass){
        String name = SecurityContextHolder.getContext().getAuthentication().getName();
        User user = repository.findByUsername(name);
        user.setPassword(encoder.encode(pass));
        repository.save(user);
    }



}
