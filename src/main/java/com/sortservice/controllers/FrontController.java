package com.sortservice.controllers;

import com.sortservice.domain.User;
import com.sortservice.domain.UserRepository;
import com.sortservice.domain.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

/**
 * Created by nick on 12.08.14.
 */
@RestController
@RequestMapping("/")
public class FrontController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private PasswordEncoder encoder;

    @RequestMapping(value = "register",method = RequestMethod.POST,consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Long register(@RequestParam("login") String login, @RequestParam("pass") String password, HttpServletResponse res){
        boolean firstUser = 0 == userRepository.count() ? true : false;
        password = encoder.encode(password);
        User newUser = new User(login, password, true);
        if(!firstUser){
           UserRole userRole = new UserRole("ROLE_USER",newUser.getUsername());
            newUser.setRoles(new ArrayList<UserRole>(){{
                add(userRole);
            }});
           return userRepository.save(newUser).getId();
        }else{
            UserRole adminRole = new UserRole("ROLE_ADMIN",newUser.getUsername());
            newUser.setRoles(new ArrayList<UserRole>(){{
                add(adminRole);
            }});
            return userRepository.save(newUser).getId();
        }
    }
}
