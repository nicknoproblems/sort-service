package com.sortservice.controllers;

import com.sortservice.domain.Order;
import com.sortservice.domain.User;
import com.sortservice.domain.UserRepository;
import com.sortservice.services.IOrderService;
import com.sortservice.services.ISortingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by nick on 16.08.14.
 */
@RestController
@RequestMapping(value = "api/sort/",consumes = MediaType.APPLICATION_JSON_VALUE)
public class SortController {
    @Autowired
    @Qualifier("lsd")
    private ISortingService lsdSorter;
    @Autowired
    @Qualifier("merge")
    private ISortingService bubbleSorter;
    @Autowired
    @Qualifier("merge")
    private ISortingService mergeSorter;


    @Autowired
    private IOrderService orderService;
    @Autowired
    private UserRepository userRepository;

    @RequestMapping(value = "lsd",method = RequestMethod.POST)
    public int[] lsdSort(@RequestBody int[] body){
        int[] result = lsdSorter.sort(body);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username= (String) auth.getPrincipal();
        User user = userRepository.findByUsername(username);
        orderService.createOrder(user, result, Order.Product.LSDSORT);
        return result;
    }

    @RequestMapping(value = "bubble",method = RequestMethod.POST)
    public int[] bubbleSort(@RequestBody int[] body){
        int[] result = bubbleSorter.sort(body);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username= (String) auth.getPrincipal();
        User user = userRepository.findByUsername(username);
        orderService.createOrder(user, result, Order.Product.BUBBLESORT);
        return result;
    }

    @RequestMapping(value = "merge",method = RequestMethod.POST)
    public int[] mergeSort(@RequestBody int[] body){
        int[] result = mergeSorter.sort(body);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username= (String) auth.getPrincipal();
        User user = userRepository.findByUsername(username);
        orderService.createOrder(user, result, Order.Product.MERGESORT);
        return result;
    }
}
