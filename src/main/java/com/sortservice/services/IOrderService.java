package com.sortservice.services;

import com.sortservice.domain.Order;
import com.sortservice.domain.User;

/**
 * Created by nick on 14.08.14.
 */
public interface IOrderService {
    User createOrder(User user, int[] result,Order.Product product);
}
