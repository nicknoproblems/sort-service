package com.sortservice.services;

import org.springframework.stereotype.Service;

/**
 * Created by nick on 12.08.14.
 */
@Service("lsd")
public class LSDRadixSort implements ISortingService {
    private final static int BITS_PER_BYTE = 8;

    @Override
    public int[] sort(int[] a) {
        int BITS = 32;
        int W = BITS / BITS_PER_BYTE;
        int R = 1 << BITS_PER_BYTE;
        int MASK = R - 1;

        int N = a.length;
        int[] aux = new int[N];

        for (int d = 0; d < W; d++) {
            int[] count = new int[R+1];
            for (int i = 0; i < N; i++) {
                int c = (a[i] >> BITS_PER_BYTE*d) & MASK;
                count[c + 1]++;
            }
            for (int r = 0; r < R; r++)count[r+1] += count[r];

            if (d == W-1) {
                int shift1 = count[R] - count[R/2];
                int shift2 = count[R/2];
                for (int r = 0; r < R/2; r++)
                    count[r] += shift1;
                for (int r = R/2; r < R; r++)
                    count[r] -= shift2;
            }

            for (int i = 0; i < N; i++) {
                int c = (a[i] >> BITS_PER_BYTE*d) & MASK;
                aux[count[c]++] = a[i];
            }

            for (int i = 0; i < N; i++)a[i] = aux[i];
        }
        return aux;
    }
}
