package com.sortservice.services;

/**
 * Created by nick on 12.08.14.
 */
public interface ISortingService {

    int[] sort(int [] list);

    
}
