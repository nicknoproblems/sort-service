package com.sortservice.services;

import org.springframework.stereotype.Service;

/**
 * Created by nick on 12.08.14.
 */
@Service("bubble")
public class BubbleSort implements ISortingService {
    @Override
    public int[] sort(int[] arr) {
        boolean swapped = true;
        int j = 0;
        int tmp;
        while (swapped) {
            swapped = false;
            j++;
            for (int i = 0; i < arr.length - j; i++) {
                if (arr[i] > arr[i + 1]) {
                    tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    swapped = true;
                }
            }
        }
        return arr;
    }
}
