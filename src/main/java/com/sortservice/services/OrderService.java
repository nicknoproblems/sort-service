package com.sortservice.services;

import com.sortservice.domain.Order;
import com.sortservice.domain.OrderRepository;
import com.sortservice.domain.User;
import com.sortservice.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by nick on 14.08.14.
 */
@Service
public class OrderService implements IOrderService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private EntityManager em;

    @Transactional
    public User createOrder(User user, int[] result,Order.Product product) {
        List<Integer> persistentList = new ArrayList<>(result.length);
        Arrays.stream(result).forEach((it)->persistentList.add(it));
        ArrayList<Order.Product> products = new ArrayList<Order.Product>() {{
            add(product);
        }};
        Order order = new Order(products, Date.from(Instant.now()), persistentList);
        order.setUser(user);
        orderRepository.save(order);
        //TypedQuery<User> query = em.createQuery("SELECT u FROM sorting_user u JOIN FETCH u.orders WHERE u.id = " + user.getId(), User.class);
        //List<User> users = query.getResultList();
        if(null == user.getOrders())
            user.setOrders(new ArrayList<Order>(){{
                add(order);
            }});
        else user.getOrders().add(order);
        return userRepository.save(user);
//        em.refresh(user, LockModeType.OPTIMISTIC);
//        em.refresh(order,LockModeType.OPTIMISTIC);
//        return user;
    }
}
