package com.sortservice.services;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by nick on 12.08.14.
 */
public class SortingAlghoritmTests {

    int [] sortingArray;

    @Before
    public void setup(){
        sortingArray = new int [1000];
        Random random = new Random(1_000_000);
        for (int i = 0; i < sortingArray.length; i++) {
            sortingArray[i] = random.nextInt();
        }
    }

    @Test
    public void bubbleSortTest(){
        ISortingService sorter = new BubbleSort();
        int[] javaSortingArray = sortingArray.clone();
        sorter.sort(sortingArray);
        Arrays.sort(javaSortingArray);
        Assert.assertArrayEquals(sortingArray,javaSortingArray);
    }

    @Test
    public void mergeSortTest(){
        ISortingService sorter = new MergeSort();
        int[] javaSortingArray = sortingArray.clone();
        sorter.sort(sortingArray);
        Arrays.sort(javaSortingArray);
        Assert.assertArrayEquals(sortingArray,javaSortingArray);
    }

    @Test
    public void lsdSortTest(){
        ISortingService sorter = new LSDRadixSort();
        int[] javaSortingArray = sortingArray.clone();
        sorter.sort(sortingArray);
        Arrays.sort(javaSortingArray);
        Assert.assertArrayEquals(sortingArray,javaSortingArray);
    }

}
